　眠れない夜、彼女はそっと恋人の腕から抜け出してリビングに向かい、テレビをつけた。
　何を見るかも決めないまま、なんとなく配信サービスを眺めて気になっていたホラー映画を選ぶ。
　幸い、明日は休日だ。寝坊しても構わないだろう。怖がりな恋人の前では見ることができない映画を、一人で楽しむのも悪くないではないか。
　さほど期待していなかったのだが、映画のできは良かった。手に汗を握る展開、巧みな演出。これは怖い。一人で観始めてしまったことを、後悔しそうになるほどだった。
　だからこそ背後から肩を叩かれた瞬間、彼女は悲鳴をあげてしまったのだ。
「うわっ」
　その途端、怯えた顔をして恋人がしゃがみこんだ。
「えっ、アオイくん！？」
　映画を止めて彼女が立ち上がると
「当たり前だろ、他に誰がいるんだよ……？」
　恨めしそうな顔をして、恋人が彼女のほうを見る。
「マジでびびった……いつの間にかお前がいなくなってるし……焦って探したら暗い部屋で一人でホラーなんかみてるし。ああーもうやめてくれよこういうの」
　ごめんねと言いながら彼女は明かりをつけ、そこで恋人の顔色があまりに悪いことに気づく。血の気のひいた頬、落ち着きなく動き回る目。
「大丈夫……？」
　思わず手を伸ばし、落ちかかる前髪をかきあげながら恋人の額に触れる。湿った感触。汗をかいているのだ。
　熱帯夜とはいえ、エアコンの効いた室内で、こんなに汗を?
　彼女はその時初めて気がついた。
（本当に怖いんだ。怖くてたまらないんだ。）
（ホラーが嫌いな人はいっぱいいるけど、アオイくんのこの怯えようはもっと……）
　だから彼女は、尋ねてしまった。
「アオイくんて、なんでそんなにホラーが嫌いなの？」
「え……？　いや、大した理由じゃないよ。怖いから嫌い。それだけ。かっこ悪いけどさ」
　口調は普段どおり。声も落ち着いて聞こえる。けれど残念ながら、彼女は既に恋人のことをよく知っていた。
　これは演技だ。アオイくんはこの話が嫌で、本気で切り上げたがっている。話すのはもちろん、考えることも嫌なくらいに。だからわざと平気なふりをしている。
　これ以上はきかないほうがいいのかもしれない。
　彼女はそう思ったのに。
　沈黙をどう受け取ったのか、恋人は深いため息をついた。
「お前、気づいちゃうんだな？　まあいいや。一度は誰かに話したかったし、だったらその相手は、お前がいい」
　ソファに腰をおろし、ぽんと横を叩く仕草。
　彼女は素直に従い、恋人の隣に座った。
「ありがとう。話せると思うと少しホッとするな。ひとりで抱えているのも、もう嫌になっちゃって」
　長い腕が彼女の体を抱え込み、それから恋人はゆっくりと話し始めた。

　いつからか、わからないんだけど。
　たぶん小学校入ったくらいかな？　もしかしたらもっと前かもしれない。でもたぶん、もっと小さい頃にはそれがおかしいってことに気づいてなかったんだ。
　ああこれは変なんだ、これは俺だけに見えているんだって、理解したのが小学生の時。
　ドッペルゲンガーって知ってる？　うん、そう。自分そっくりの分身。実際に出会っちゃうと死ぬとかいうやつ。
　アレだと思うんだよな。わからない。そこまでそっくりってわけじゃないから。
　そう、いるんだよ、俺によく似たやつがさ。いつもじゃないけど、時々いる。
　たとえば駅で、隣のホームに立っていたり。
　教室の窓から見上げた屋上の、フェンスに寄りかかっていたり。
　コンビニの前を通りかかると、立ち読みしていたりすることもあったな。
　俺は俺を、見つけてしまう。
　俺とはちょっと違うところもあるんだけどな。あいつ、としか言いようがないんだけど。とにかくやつは、俺よりも少し若いんだ。たぶん二、三歳。向こうのほうが年下。
　ああ、そうだよ。俺と一緒にあいつも成長してる。最初見たときは子供だったけど、今はもう大人。たぶんこのままなら、一緒に中年になって、老人になるんだろうな。
　顔や体型も、少し違うかも。でも他人にしては似過ぎている。
　怖かったよ。正直に言えば、今も怖いよ。最後にあいつを見てからだいぶ経つけど、それでももしかしたら今日、また見つけてしまうかもしれないって、思っている。
　窓の向こうに。鏡を覗き込んでいる俺の後ろに。ドアを開けたらそこに。あいつはいるかもしれない。
　……笑わないな。お前のそういうところ、助かるよ。つくり話だとも思わないんだな？　それも助かる。ありがとう。
　ドッペルゲンガーという言葉を知って、見れば死ぬなんて話をきいたときは、しばらくまともじゃいられなかったな。まだ子供だったし。いや、大人だって怖いか。俺は死ぬんだ、あいつに殺されるんだって、本気で思ったよ。
　自分がまだ死にそうじゃないことに気づいて少し気が楽になって、でもあいつを見つけるたび、叫びだしそうになった。今までは見逃されていたとしても、今日こそはあいつにやられるかもしれないって。でも誰も、あいつが見えないみたいで。助けてくれって言っても、みんなわかってくれない。きょとんとした顔をされて、笑われることもあって。
　だから、やめたんだ。誰かに話すのも、わかってもらおうとするのも。両親だって、このことは知らない。ちゃんと話したのは今、お前が初めて。
　気のせい？　うん、それが一番いい考えだ。普段は俺も、そう思うようにしてる。
　それにあいつ、今ではそれほど俺に似てないしな。
　そうなんだ、髪を染めてピアスをあけたのは、それもあるんだ。あいつは髪は染めてないから。ピアスもしてない。なんだろう……俺よりも真面目なカンジ？
　もしもあいつまで髪を染めたらどうしようって、最初はびくびくしてたけどな。そういうことはなかった。あいつの髪はもとのままで、長さももっと短い。おかげで俺とは別人だって、そう思いやすくなったよ。
　別人だとしても、怖いんだけどさ。だってどこに行っても、あいつは出てくるんだぜ？　旅先で見かけたこともある。海外でもお構いなし。完全な他人だとしたら、それはそれですごく異常なやつにつきまとわれているってことになる。
　例外は、あの島くらいだな。うん、あそこでは見なかった。俺、お前に言っただろ「楽な生活だ」って。色んな意味で楽だったけど、あいつに会う心配をしないで済むのは、けっこう大きかったな。
　俺の頭がおかしいんだと思う？　違うのか。それはありがたい……のかな？　わからない。どっちがいいのかな。あいつの正体は俺の個人的な幻覚で、俺の頭がおかしいだけっっていうほうが、もしかしてマシな気もして。
　あのさ、お前知ってるよな。俺が生まれる前にあったこと。そう、俺は親父が浮気してできた子だって話。ちゃんとした婚約者が他にいたってやつね。うん。
　俺その話を初めて知った時、思ったんだよね。ああそうか、本当はあいつがカガミアオイだったんだろうなって。名前は違うかもしれないけどさ。
　そう考えると、あいつが少し年下なのもなんでかわかるんだよ。俺は予定外の、いわばフライングで生まれた子供だろ？　本当はあいつの年が正しいんだよ。順番通りに物事が進めば、加賀見の家の長男は、二年か三年遅く生まれたってことなんじゃないか？
　ドッペルゲンガー。もうひとりの自分。あいつはきっと、俺より正しいカガミアオイ。
　隠れて必死に勉強しなくたって本当に優秀で。もっと堂々としていて。あの家を継ぐ器がないなんて、そんなことに悩む必要がないんだ。
　だから髪も染めないし、ピアスもあけないし、家を飛び出したりしない。実際あいつ、そういう風に見えるよ。俺よりずっと、優秀そうに。
　……ああ大丈夫。落ち込んでるわけじゃない。今は、もう平気なんだ。
　うん、昔は駄目だった。いつも責められている気がしたよ。実際あの頃は、あいつを毎日のように見ていたしな。
　間違ったお前が、なぜそこにいるって。
　無能のくせに有能ぶって、それも貫けなくて。いきがって一人前のつもりで、そのくせ何もうまくいかない。
　情けないやつ。見掛け倒し。俺ならもっとうまくやるのに。
　だから、早く、そこを譲れ。
　そう思われているのがわかった。
　……怖がりなのは元々だけどさ。そういう、情けなくてかっこ悪いやつだから俺、本当は。もう知ってるだろ？
　怯えている俺を見てあいつがどう思うのか考えると、ますます苦手になってさ。嗤われるならまだいいけど、怒っているかもしれないって。
　もっと立派な自分になりたかった。あいつに嗤われないような、怒られないような。あいつを押しのけて生まれた俺なのに。こんなんじゃそりゃ怒るよ。わかっているのに、できなくて。
　だから一度、うん、ちょっと危なかったことがあって。本気で死のうとしたのとは違うんだけど、逃げたくて、消えたくて、それで……。
　そんな顔するなよ。昔のことだって。今じゃない。あの時だけだし、もう二度とやらない。絶対に。
　だってさ、夢にあいつが出てきたんだ。
「なにやってるんだ」
　って。怒ってたなあれは。
　それを見たら俺はもう、どうしていいかわからなくなってさ。
　気がついたら、
「代わってくれ」
　と頼んでた。
　お前のほうがうまくやれる、俺なんかじゃなく、お前がここに来てくれって。
　そしたら、
「その願いは簡単なことじゃない。いずれ幸せだと思える日は来るから、それを待て。それまではお前は、ちゃんと生きてろ」
　って。
　そんなこと言われるなんて、すごくびっくりした。ただ憎まれているだけだと思っていたからな。
　でもそれで、少しずつ気持ちが変わったんだ。あいつが言うなら本当かもしれないって、気がしてさ。ちゃんとしないと怒られている気がするのは相変わらずだったし、やっぱり俺は怖がりのままだったけど。幸せなんて本当にあるのかって、疑ってしまうのは変わらなかったし。
　言っただろう。全部昔のことなんだ。あいつのことも、もうずっと見てないんだ。だからお前に話したかった。
　お前がいるだけで、何もかも違う。満たされる。
　この俺でもいいんだって、思える。臆病で見栄っ張りで情けないのは変わらなくても、そういう俺でもいいんだって。
　そう、今はもう幸せなんだよ。あいつが言っていたのはこれなんだって、ようやくわかったんだ。
　ありがとう。
　愛してる。

　そうやって、その話は終わった。
　ホラー映画は中断されたまま、彼女は恋人と同じベッドに戻った。たくさんのキスとハグ。
　幸せだ、と彼女も思う。愛してると感じる。おそらくは恋人と同じように。
　カガミアオイがもがいて、苦しみながら、求めていた幸福。
　まぶたの裏で白い光点が明滅し、意識が消えるその直前、彼女の脳裏をよぎった疑問があった。
（もうひとりのアオイくんは、どうしてアオイくんに幸せになってほしかったの？）
（アオイくんが幸せになると、何が起きるの？）
　けれどその先を彼女は、考えられなかった。

　翌朝、彼女の恋人がいつものように
「おはよう」
　と声をかけてきた。すぐに
「美容室の予約とれたから、行ってくる」
　と微笑む。
　帰宅した彼の髪は短く切られ、色は暗く変わっている。
　いつの間にか恋人は、ピアスをつけなくなった。お気に入りだったはずのものもしまいこみ、手に取ることすらない。
　話し方が以前と違うと、そう感じる時がある。はっきりと指摘できるような何かがあるわけじゃないのだけど。
　ホラー映画を嫌がらない。並んで一緒に見たりする。余裕のある笑みを浮かべ、彼女の髪を撫でながら。
　そうして彼はうっとりと目を細め、言うのだ。
「幸せだ」
「ずっと待っていた」
「君のおかげだ」
　と。

　カガミアオイは、かわったのだ。


「しばらく前から知らないあなた」/End